<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package azera-shop
 */

get_header();

azera_shop_wrapper_start( 'col-md-12 post-list', true ); 
?>

	<main <?php if ( have_posts() ) { echo 'itemscope itemtype="http://schema.org/Blog"';} ?> id="main" class="site-main" role="main">
		<div class="welcome-block">
			<div class="pointer_logo">&nbsp;</div>
			<div  class="header-intro">
				<h1 class="header-navy">Witamy na naszej stronie</h1>
				<p class="text-navy text-intro">
					Państwowa Szkoła Muzyczna I st. w Gdańsku im. H.Wieniawskiego jest placówką kształcenia artystycznego na poziomie podstawowym. Szkoła istnieje od 1947 roku i przez cały czas swojej działalności znacząco spełnia misję propagatora sztuki w Gdańsku-Oruni. Osiągnięcia szkoły zostały nagrodzone przyznaniem prestiżowego Medalu 1000-lecia Gdańska (w 1997 roku, z okazji 50 - lecia istnienia).
				</p>
				<p class="text-navy text-intro">
					W grudniu 2007 roku szkoła obchodziła jubileusz 60-lecia. Przyznano wiele nagród i odznaczeń m. in. Odznakę Gryfa Pomorskiego, Medale Wojewody Pomorskiego Sint sua premia laudi, Zasłużony dla Kultury Polskiej, Nagrody i Listy Gratulacyjne Ministra Kultury i Dziedzictwa Narodowego oraz Prezydenta Miasta Gdańska.
				</p>
				<p class="text-navy text-intro">
					Obecnie do szkoły uczęszcza 160 uczniów. Swoją wiedzę muzyczną oraz doświadczenie przekazuje im 21 pedagogów. Uczniowie nasi mogą gruntownie rozwinąć swoje uzdolnienia muzyczne, aby dalej kontynuować naukę w szkołach muzycznych II stopnia. Nauczanie prowadzone jest w oparciu o cykl 6-cio i 4-letni. Warunkiem ubiegania się kandydata o przyjęcie do klasy I jest ukończenie 6 lat i nie przekroczenie 16 roku życia.
				</p>
			</div>
		</div>
		<div class="section-title"><h1 class="header-navy">Akutalności / Wydarzenia</h1></div>	
		<?php if ( have_posts() ) : ?>

			<?php /* Start the Loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>

				<?php
				get_template_part( 'content' ); ?>

			<?php endwhile; ?>

			<?php echo apply_filters( 'azera_shop_post_navigation_filter', get_the_posts_navigation() ); ?>

		<?php else : ?>

			<?php get_template_part( 'content', 'none' ); ?>

		<?php endif; ?>
		<!--
		<div class="section-title-center"><h1 class="header-navy">GALERIA</h1></div>
		-->
		<div class="section-title-center"><h1 class="header-navy">MAPKA</h1></div>
		<div class="mapka_main">&nbsp;</div>

	</main><!-- #main -->

<?php
azera_shop_wrapper_end( false );
get_footer(); ?>
