<?php
function my_theme_enqueue_styles() {

    $parent_style = 'parent-style'; 

    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array( $parent_style ),
        wp_get_theme()->get('Version')
    );
}
add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );


if ( ! function_exists( 'azera_shop_post_date_box_function' ) ) {

	/**
	 * Function to create the box with the post date
	 */
	function azera_shop_post_date_box_function( $class ) {
		?>
		<div class="<?php if ( ! empty( $class ) ) { echo $class; } ?>">
			<span class="post-date-day"><?php $tday = get_post_custom_values('dd');  if(! $tday[0]){ the_time( _x( 'd', 'post day fomat', 'azera-shop' ));}else{echo $tday[0];} ?></span>
			<span class="post-date-month">
				<?php 
						$tmonth = get_post_custom_values('mm');  
						if(! $tmonth[0]){ 
								the_time( _x( 'F', 'post month fomat', 'azera-shop' )); echo " "; 
							}
						else{echo $tmonth[0];}
						
						echo " "; 
						
						$tyear = get_post_custom_values('yy');  
						if(! $tyear[0]){
								the_time( _x( 'Y', 'post month fomat', 'azera-shop' ));
							}
						else{echo $tyear[0];} 				
				?>
			</span>
		</div>
		<?php
	}
}
add_action( 'azera_shop_post_date_box','azera_shop_post_date_box_function', 10, 1 );

// Loads the child theme textdomain.

function azera_shop_child_locale() {
    load_child_theme_textdomain( 'azera_shop', get_stylesheet_directory() . '/languages' );
}
add_action( 'after_setup_theme', 'azera_shop_child_locale' );




/**
 * Wrapper start for content.
 *
 * @param string $class     class name.
 * @param bool   $is_blog     if is blog.
 */
function azera_shop_wrapper_start_narrow( $class = 'col-md-9', $is_blog = false ) {
	$page_bg_image_url = get_background_image();
	$class_to_add = '';
	if ( ! empty( $page_bg_image_url ) && ! is_page_template( 'template-frontpage.php' ) ) {
		$class_to_add = 'content-background';
	} ?>
	</div>
	</header>
	<?php
	if ( $is_blog == true ) {
		do_action( 'blog_header' );
	} ?>
	<div class="content-wrap">
	<div class="container <?php echo $class_to_add; ?>">
	<div id="primary" class="content-area <?php echo esc_attr( $class ); ?>">
	<?php
}
add_action( 'woocommerce_before_main_content', 'azera_shop_wrapper_start_narrow', 10 );